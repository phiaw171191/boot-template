package com.ifc;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableConfigurationProperties
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.ifc.loop.repo")
public class LOOP_DBConfig {

	/**
	 * auto read from config jndi-name
	 */
	@Bean(destroyMethod = "")
	@ConfigurationProperties(prefix = "spring.datasource.loop")
	public JndiPropertyHolder primary() {
		return new JndiPropertyHolder();
	}

	/**
	 * Init datasource
	 */
	@Bean(destroyMethod = "", name = "loopDataSource")
	@Primary
	public DataSource primaryDataSource() {
		JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
		DataSource dataSourceLOOP = dataSourceLookup.getDataSource(primary().getJndiName());
		return dataSourceLOOP;
	}
	
	/**
	 * Init package and model/domain
	 */
	@Bean(name = "entityManagerFactory")
    @Primary
    public LocalContainerEntityManagerFactoryBean emf1(EntityManagerFactoryBuilder builder){
        return builder
                .dataSource(primaryDataSource())
                .packages("com.ifc.loop.model")
                .persistenceUnit("Candidate")
                .persistenceUnit("City")
                .build();
    }
}

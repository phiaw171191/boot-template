package com.ifc;

public class JndiPropertyHolder {

	private String jndiName;

	public String getJndiName() {
		return jndiName;
	}

	public void setJndiName(String jndiName) {
		this.jndiName = jndiName;
	}
}

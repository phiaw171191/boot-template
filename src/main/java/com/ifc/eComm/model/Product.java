package com.ifc.eComm.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;
import org.hibernate.validator.constraints.NotBlank;

@Entity
//@Table(name = "Product")
public class Product {
	@Id
	@GeneratedValue
	private Long id;

	@NotBlank
	@Index(name = "idx_contractType")
	@Column(length = 6, nullable = false)
	private String contract_Type;

	@Column(length = 10)
	private String non_Forfeiture_Option;

	private boolean renewable;

	@NotBlank
	@Column(length = 30, nullable = false)
	private String product_Name;

	//@NotNull
	/**
	 LAZY = fetch when needed
	 EAGER = fetch immediately
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "basic_plan")
	@ForeignKey(name = "fk_plan5")
	private Plan basic_Plan;

	private float min_Annual_Premium;

	@Column(nullable = false)
	private String default_Campaign_Code;

	@NotNull
	@Column(columnDefinition = "datetime", nullable = false)
	private Date create_Time;

	@Column(length = 100)
	private String awpl_Form_Template;

	@Column(length = 50)
	private String awpl_Form_Class;

	@Column(length = 50)
	private String awpl_Form_Category;

	@Column(length = 50)
	private String awpl_Form_Document_Id;

	@Column(length = 50)
	private String awpl_Form_Sub_Category;

	private Integer awpl_Form_Trigger;

	@Column(length = 10)
	private String awpl_Form_Print;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContract_Type() {
		return contract_Type;
	}

	public void setContract_Type(String contract_Type) {
		this.contract_Type = contract_Type;
	}

	public String getNon_Forfeiture_Option() {
		return non_Forfeiture_Option;
	}

	public void setNon_Forfeiture_Option(String non_Forfeiture_Option) {
		this.non_Forfeiture_Option = non_Forfeiture_Option;
	}

	public boolean isRenewable() {
		return renewable;
	}

	public void setRenewable(boolean renewable) {
		this.renewable = renewable;
	}

	public String getProduct_Name() {
		return product_Name;
	}

	public void setProduct_Name(String product_Name) {
		this.product_Name = product_Name;
	}

	public Plan getBasic_Plan() {
		return basic_Plan;
	}

	public void setBasic_Plan(Plan basic_Plan) {
		this.basic_Plan = basic_Plan;
	}

	public float getMin_Annual_Premium() {
		return min_Annual_Premium;
	}

	public void setMin_Annual_Premium(float min_Annual_Premium) {
		this.min_Annual_Premium = min_Annual_Premium;
	}

	public String getDefault_Campaign_Code() {
		return default_Campaign_Code;
	}

	public void setDefault_Campaign_Code(String default_Campaign_Code) {
		this.default_Campaign_Code = default_Campaign_Code;
	}

	public Date getCreate_Time() {
		return create_Time;
	}

	public void setCreate_Time(Date create_Time) {
		this.create_Time = create_Time;
	}

	public String getAwpl_Form_Template() {
		return awpl_Form_Template;
	}

	public void setAwpl_Form_Template(String awpl_Form_Template) {
		this.awpl_Form_Template = awpl_Form_Template;
	}

	public String getAwpl_Form_Class() {
		return awpl_Form_Class;
	}

	public void setAwpl_Form_Class(String awpl_Form_Class) {
		this.awpl_Form_Class = awpl_Form_Class;
	}

	public String getAwpl_Form_Category() {
		return awpl_Form_Category;
	}

	public void setAwpl_Form_Category(String awpl_Form_Category) {
		this.awpl_Form_Category = awpl_Form_Category;
	}

	public String getAwpl_Form_Document_Id() {
		return awpl_Form_Document_Id;
	}

	public void setAwpl_Form_Document_Id(String awpl_Form_Document_Id) {
		this.awpl_Form_Document_Id = awpl_Form_Document_Id;
	}

	public String getAwpl_Form_Sub_Category() {
		return awpl_Form_Sub_Category;
	}

	public void setAwpl_Form_Sub_Category(String awpl_Form_Sub_Category) {
		this.awpl_Form_Sub_Category = awpl_Form_Sub_Category;
	}

	public Integer getAwpl_Form_Trigger() {
		return awpl_Form_Trigger;
	}

	public void setAwpl_Form_Trigger(Integer awpl_Form_Trigger) {
		this.awpl_Form_Trigger = awpl_Form_Trigger;
	}

	public String getAwpl_Form_Print() {
		return awpl_Form_Print;
	}

	public void setAwpl_Form_Print(String awpl_Form_Print) {
		this.awpl_Form_Print = awpl_Form_Print;
	}

	
}

package com.ifc.eComm.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "individual_plan")
public class Plan {

	@Id
	@Column(length = 8)
	private String plan_Code;

	@NotEmpty
	@Column(columnDefinition = "nvarchar(100)")
	private String name;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(length = 30, nullable=false)
	private PlanStatus status;

	@Column(columnDefinition = "datetime")
	private Date launch_Time;

	@Column(columnDefinition = "datetime")
	private Date withdraw_Time;

	private Integer min_Entry_Age;

	private Integer max_Entry_Age;

	@Column(length = 100)
	private String pay_Freq;

	@Column(length = 100)
	private String occupation_Classes;
	
	// --------- start: sum assured ----------
	// Hibernate doesnt recommend to separate these into a secondary table,
	// please read "Mapping one entity to several tables" or search
	// @SecondaryTable
	private Integer min_Sum_Assured;

	private Integer max_Sum_Assured;

	private Integer sum_Assured_Count;
	
	@Column(name = "pct_basic_sum_assured")
	private Integer percentage_Of_Basic_Sum_Assured;
	
	@Column(length = 10)
	private String sum_Assured_Ccy;
	// --------- end: sum assured ----------

	private Integer min_Premium_Per_Month;
	
	private int prem_Cess_Age;
	
	private int risk_Age;
	
	private boolean hidden;

	public String getPlan_Code() {
		return plan_Code;
	}

	public void setPlan_Code(String plan_Code) {
		this.plan_Code = plan_Code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public PlanStatus getStatus() {
		return status;
	}

	public void setStatus(PlanStatus status) {
		this.status = status;
	}

	public Date getLaunch_Time() {
		return launch_Time;
	}

	public void setLaunch_Time(Date launch_Time) {
		this.launch_Time = launch_Time;
	}

	public Date getWithdraw_Time() {
		return withdraw_Time;
	}

	public void setWithdraw_Time(Date withdraw_Time) {
		this.withdraw_Time = withdraw_Time;
	}

	public Integer getMin_Entry_Age() {
		return min_Entry_Age;
	}

	public void setMin_Entry_Age(Integer min_Entry_Age) {
		this.min_Entry_Age = min_Entry_Age;
	}

	public Integer getMax_Entry_Age() {
		return max_Entry_Age;
	}

	public void setMax_Entry_Age(Integer max_Entry_Age) {
		this.max_Entry_Age = max_Entry_Age;
	}

	public String getPay_Freq() {
		return pay_Freq;
	}

	public void setPay_Freq(String pay_Freq) {
		this.pay_Freq = pay_Freq;
	}

	public String getOccupation_Classes() {
		return occupation_Classes;
	}

	public void setOccupation_Classes(String occupation_Classes) {
		this.occupation_Classes = occupation_Classes;
	}

	public Integer getMin_Sum_Assured() {
		return min_Sum_Assured;
	}

	public void setMin_Sum_Assured(Integer min_Sum_Assured) {
		this.min_Sum_Assured = min_Sum_Assured;
	}

	public Integer getMax_Sum_Assured() {
		return max_Sum_Assured;
	}

	public void setMax_Sum_Assured(Integer max_Sum_Assured) {
		this.max_Sum_Assured = max_Sum_Assured;
	}

	public Integer getSum_Assured_Count() {
		return sum_Assured_Count;
	}

	public void setSum_Assured_Count(Integer sum_Assured_Count) {
		this.sum_Assured_Count = sum_Assured_Count;
	}

	public Integer getPercentage_Of_Basic_Sum_Assured() {
		return percentage_Of_Basic_Sum_Assured;
	}

	public void setPercentage_Of_Basic_Sum_Assured(Integer percentage_Of_Basic_Sum_Assured) {
		this.percentage_Of_Basic_Sum_Assured = percentage_Of_Basic_Sum_Assured;
	}

	public String getSum_Assured_Ccy() {
		return sum_Assured_Ccy;
	}

	public void setSum_Assured_Ccy(String sum_Assured_Ccy) {
		this.sum_Assured_Ccy = sum_Assured_Ccy;
	}

	public Integer getMin_Premium_Per_Month() {
		return min_Premium_Per_Month;
	}

	public void setMin_Premium_Per_Month(Integer min_Premium_Per_Month) {
		this.min_Premium_Per_Month = min_Premium_Per_Month;
	}

	public int getPrem_Cess_Age() {
		return prem_Cess_Age;
	}

	public void setPrem_Cess_Age(int prem_Cess_Age) {
		this.prem_Cess_Age = prem_Cess_Age;
	}

	public int getRisk_Age() {
		return risk_Age;
	}

	public void setRisk_Age(int risk_Age) {
		this.risk_Age = risk_Age;
	}

	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}
	


}

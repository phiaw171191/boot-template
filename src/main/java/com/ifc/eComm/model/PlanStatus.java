package com.ifc.eComm.model;

public enum PlanStatus {
	LAUNCHED, WITHDRAWN;
}

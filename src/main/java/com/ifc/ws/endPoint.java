package com.ifc.ws;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ifc.eComm.model.Product;
import com.ifc.eComm.repo.ProductRepository;
import com.ifc.loop.model.Candidate;
import com.ifc.loop.model.City;
import com.ifc.loop.repo.CandidateRepository;
import com.ifc.loop.repo.CityRepository;
import com.ifc.rre.model.Fwd_Agent;
import com.ifc.rre.repo.FWDAgentRepository;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "ifc")
public class endPoint {

	@Autowired
	CandidateRepository candidateRepo;
	
	@Autowired
	FWDAgentRepository agentRepo;

	@Autowired
	ProductRepository productRepo;
	
	@Autowired
	CityRepository cityRepo;
	

	@ApiOperation(value = "pick Up Candidate ", notes = "pick Up Candidate ")
	@PutMapping(value = "/getCandidate/{candidate_id}")
	@ResponseBody
	@Transactional
	public Map<String, Object> getCandidate(@PathVariable("candidate_id") Long id) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			Candidate candidate = candidateRepo.findByCandidateId(id);
			//Fwd_Agent agent = agentRepo.findByAgentCode("10000730");
			// map.put("email", candidate.getEmail());
			//map.put("agent", agent)
			map.put("data", candidate);
			map.put("status", "Y");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			map.put("status", "N");
			map.put("errorCode", "001");
			map.put("message", "system error");
			return map;

		}

		return map;
	}

	@ApiOperation(value = "Get All Candidate ", notes = "Get All Candidate")
	@PostMapping(value = "/getAllCandidate")
	@ResponseBody
	//@Transactional
	public List<Candidate> getAllCandidate() {

		List<Candidate> candidate = candidateRepo.findAll();

		return candidate;
	}

	@ApiOperation(value = "Get 5 agent ", notes = "Get 5 agent")
	@PostMapping(value = "/get5Agent")
	@ResponseBody
	public List<Fwd_Agent> getAgent5() {

		List<Fwd_Agent> agent = agentRepo.findAll();

		return agent;
	}
	
	@ApiOperation(value = "getAgentByCode", notes = "getAgentByCode")
	@PutMapping(value = "/getAgentByCode/{agentCode}")
	@ResponseBody
	public Map<String, Object> getAgentByCode(@PathVariable("agentCode") String agentCode) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			List<Fwd_Agent> agent = agentRepo.findByAgentCode(agentCode);
			// map.put("email", candidate.getEmail());
			map.put("data", agent);
			map.put("status", "Y");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			map.put("status", "N");
			map.put("errorCode", "001");
			map.put("message", "system error");
			return map;

		}

		return map;
	}
	
	@ApiOperation(value = "Get Product", notes = "Get Product")
	@GetMapping(value = "/product")
	@ResponseBody
	public List<Product> getProduct() {

		List<Product> Product = productRepo.findAll();

		return Product;
	}
	
	@ApiOperation(value = "Get City", notes = "Get City")
	@GetMapping(value = "/city")
	@ResponseBody
	public List<City> getCity() {

		List<City> City = cityRepo.findAll();

		return City;
	}
	
}

package com.ifc;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableConfigurationProperties
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = "com.ifc.rre.repo", entityManagerFactoryRef = "emf2")
public class RRE_DBConfig {

	/**
	 * auto read from config jndi-name
	 */
	@Bean(destroyMethod = "")
	@ConfigurationProperties(prefix = "spring.datasource.rre")
	public JndiPropertyHolder secondary() {
		return new JndiPropertyHolder();
	}

	@Bean(destroyMethod = "", name = "rreDataSource")
	public DataSource secondaryDataSource() {
		JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
		DataSource dataSourceRRE = dataSourceLookup.getDataSource(secondary().getJndiName());
		return dataSourceRRE;
	}

	@Bean
    public LocalContainerEntityManagerFactoryBean emf2(EntityManagerFactoryBuilder builder){
        return builder
                .dataSource(secondaryDataSource())
                .packages("com.ifc.rre.model")
                .persistenceUnit("Fwd_Agent")
                .build();
    }

	

}

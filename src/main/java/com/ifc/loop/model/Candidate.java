package com.ifc.loop.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "Candidate")
public class Candidate {

	@Id
	@GeneratedValue
	@Column(name = "candidate_id")
	private Long candidateId;

	@Column(length = 50)
	private String sess_id;

	@Column(length = 50)
	private String first_name;

	@Column(length = 50)
	private String last_name;

	@Column(length = 16)
	private String mobile_number;

	@Column(length = 50)
	private String email;

	@Column(length = 1)
	private String preferred_gender;

	@Column(columnDefinition = "datetime")
	private Date date_of_birth;

	@Column(length = 500)
	private String preferred_city;

	@Column
	private Double meeting_latitude;

	@Column
	private Double meeting_longitude;

	@Column(columnDefinition = "datetime")
	private Date meeting_date;

	@Column(columnDefinition = "datetime")
	private Date created_date;

	@Column(columnDefinition = "datetime")
	private Date update_date;

	@Column(length = 50)
	private String referral;

	@Column(length = 100)
	private String remote_address;

	@Column
	private Long event_id;

	@Column
	private Boolean tnc;

	@Column(length = 20)
	private String sync_status;

	@Column(columnDefinition = "datetime")
	private Date last_retry_date;

	@Column
	private int retry_count;
	
	@Column(length = 50)
	private String ext_info;
	
	@Column(length = 500)
	private String meeting_address;
	
	@Column(length = 1)
	private String gender;
	
	@Column
	private Long ref_no;
	
	

	public Long getCandidate_id() {
		return candidateId;
	}

	public void setCandidate_id(Long candidate_id) {
		this.candidateId = candidate_id;
	}

	public String getSess_id() {
		return sess_id;
	}

	public void setSess_id(String sess_id) {
		this.sess_id = sess_id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getMobile_number() {
		return mobile_number;
	}

	public void setMobile_number(String mobile_number) {
		this.mobile_number = mobile_number;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPreferred_gender() {
		return preferred_gender;
	}

	public void setPreferred_gender(String preferred_gender) {
		this.preferred_gender = preferred_gender;
	}

	public Date getDate_of_birth() {
		return date_of_birth;
	}

	public void setDate_of_birth(Date date_of_birth) {
		this.date_of_birth = date_of_birth;
	}

	public String getPreferred_city() {
		return preferred_city;
	}

	public void setPreferred_city(String preferred_city) {
		this.preferred_city = preferred_city;
	}

	public Double getMeeting_latitude() {
		return meeting_latitude;
	}

	public void setMeeting_latitude(Double meeting_latitude) {
		this.meeting_latitude = meeting_latitude;
	}

	public Double getMeeting_longitude() {
		return meeting_longitude;
	}

	public void setMeeting_longitude(Double meeting_longitude) {
		this.meeting_longitude = meeting_longitude;
	}

	public Date getMeeting_date() {
		return meeting_date;
	}

	public void setMeeting_date(Date meeting_date) {
		this.meeting_date = meeting_date;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

	public String getReferral() {
		return referral;
	}

	public void setReferral(String referral) {
		this.referral = referral;
	}

	public String getRemote_address() {
		return remote_address;
	}

	public void setRemote_address(String remote_address) {
		this.remote_address = remote_address;
	}

	public Long getEvent_id() {
		return event_id;
	}

	public void setEvent_id(Long event_id) {
		this.event_id = event_id;
	}

	public Boolean getTnc() {
		return tnc;
	}

	public void setTnc(Boolean tnc) {
		this.tnc = tnc;
	}

	public String getSync_status() {
		return sync_status;
	}

	public void setSync_status(String sync_status) {
		this.sync_status = sync_status;
	}

	public Date getLast_retry_date() {
		return last_retry_date;
	}

	public void setLast_retry_date(Date last_retry_date) {
		this.last_retry_date = last_retry_date;
	}

	public int getRetry_count() {
		return retry_count;
	}

	public void setRetry_count(int retry_count) {
		this.retry_count = retry_count;
	}

	public String getExt_info() {
		return ext_info;
	}

	public void setExt_info(String ext_info) {
		this.ext_info = ext_info;
	}

	public String getMeeting_address() {
		return meeting_address;
	}

	public void setMeeting_address(String meeting_address) {
		this.meeting_address = meeting_address;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Long getRef_no() {
		return ref_no;
	}

	public void setRef_no(Long ref_no) {
		this.ref_no = ref_no;
	}	
	
	public Candidate() {
		// TODO Auto-generated constructor stub
	}
	
}

package com.ifc.loop.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ifc.loop.model.Candidate;

@Repository
public interface CandidateRepository extends JpaRepository<Candidate, Long>{

	Candidate findByCandidateId(Long candidate_id);
	
	@Query(value = " SELECT * FROM Candidate", nativeQuery = true)
	List<Candidate> findAll();

}

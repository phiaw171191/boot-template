package com.ifc.rre.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ifc.rre.model.Fwd_Agent;

@Repository
public interface FWDAgentRepository extends JpaRepository<Fwd_Agent, Long> {

	@Query(value = "SELECT TOP 5 * FROM FWD_AGENT", nativeQuery = true)
	List<Fwd_Agent> findAll();
	
	List<Fwd_Agent> findByAgentCode(String agentCode);

	/**
	 * @Query(
	 * "select u from User u where u.firstname = :firstname or u.lastname = :lastname"
	 * ) User findByLastnameOrFirstname(@Param("lastname") String
	 * lastname, @Param("firstname") String firstname);
	 */

}

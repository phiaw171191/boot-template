package com.ifc.rre.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "fwd_agent")
public class Fwd_Agent {
	
	@Id
	@GeneratedValue
	@Column(name = "agent_id")
	private Long agentId;
	
	@Column(columnDefinition = "datetime")
	private Date create_datetime;
	
	@Column(columnDefinition = "datetime")
	private Date last_modified_datetime;
	
	@Column(length = 50,name = "agent_code")
	private String agentCode;
	
	@Column
	private Float agent_latitude;
	
	@Column(length = 500)
	private String agent_location;
	
	@Column
	private Float agent_longitude;
	
	@Column(length = 100)
	private String agent_name;
	
	@Column
	private Boolean allow_change_location;
	
	@Column
	private Float distances;
	
	@Column
	private Boolean is_blacklisted;
	
	@Column(columnDefinition = "datetime")
	private Date agent_birth_datetime;
	
	@Column(length = 1)
	private String agent_gender;
	
	@Column
	private String agent_province;
	
	@Column(length = 20)
	private String agent_level;
	
	@Column(length = 50)
	private String agent_course;
	
	@Column(length = 20)
	private String agent_license;
	
	@Column(length = 20)
	private String agent_referral_name;

	@Column(columnDefinition = "datetime")
	private Date agent_last_assign_datetime;	
	
	

	public String getAgent_level() {
		return agent_level;
	}

	public void setAgent_level(String agent_level) {
		this.agent_level = agent_level;
	}

	public String getAgent_course() {
		return agent_course;
	}

	public void setAgent_course(String agent_course) {
		this.agent_course = agent_course;
	}

	public String getAgent_license() {
		return agent_license;
	}

	public void setAgent_license(String agent_license) {
		this.agent_license = agent_license;
	}

	public String getAgent_referral_name() {
		return agent_referral_name;
	}

	public void setAgent_referral_name(String agent_referral_name) {
		this.agent_referral_name = agent_referral_name;
	}

	public Date getAgent_birth_datetime() {
		return agent_birth_datetime;
	}

	public void setAgent_birth_datetime(Date agent_birth_datetime) {
		this.agent_birth_datetime = agent_birth_datetime;
	}

	public String getAgent_gender() {
		return agent_gender;
	}

	public void setAgent_gender(String agent_gender) {
		this.agent_gender = agent_gender;
	}

	public String getAgent_province() {
		return agent_province;
	}

	public void setAgent_province(String agent_province) {
		this.agent_province = agent_province;
	}

	public Date getAgent_last_assign_datetime() {
		return agent_last_assign_datetime;
	}

	public void setAgent_last_assign_datetime(Date agent_last_assign_datetime) {
		this.agent_last_assign_datetime = agent_last_assign_datetime;
	}

	public Long getAgent_id() {
		return agentId;
	}

	public void setAgent_id(Long agent_id) {
		this.agentId = agent_id;
	}

	public Date getCreate_datetime() {
		return create_datetime;
	}

	public void setCreate_datetime(Date create_datetime) {
		this.create_datetime = create_datetime;
	}

	public Date getLast_modified_datetime() {
		return last_modified_datetime;
	}

	public void setLast_modified_datetime(Date last_modified_datetime) {
		this.last_modified_datetime = last_modified_datetime;
	}

	public String getAgent_code() {
		return agentCode;
	}

	public void setAgent_code(String agent_code) {
		this.agentCode = agent_code;
	}

	public Float getAgent_latitude() {
		return agent_latitude;
	}

	public void setAgent_latitude(Float agent_latitude) {
		this.agent_latitude = agent_latitude;
	}

	public String getAgent_location() {
		return agent_location;
	}

	public void setAgent_location(String agent_location) {
		this.agent_location = agent_location;
	}

	public Float getAgent_longitude() {
		return agent_longitude;
	}

	public void setAgent_longitude(Float agent_longitude) {
		this.agent_longitude = agent_longitude;
	}

	public String getAgent_name() {
		return agent_name;
	}

	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}

	public Boolean getAllow_change_location() {
		return allow_change_location;
	}

	public void setAllow_change_location(Boolean allow_change_location) {
		this.allow_change_location = allow_change_location;
	}

	public Float getDistances() {
		return distances;
	}

	public void setDistances(Float distances) {
		this.distances = distances;
	}

	public Boolean getIs_blacklisted() {
		return is_blacklisted;
	}

	public void setIs_blacklisted(Boolean is_blacklisted) {
		this.is_blacklisted = is_blacklisted;
	}
	
	public Fwd_Agent() {
		// TODO Auto-generated constructor stub
	}
}
